export interface IStudent {
    Id: string,
    Group: string,
    Name: string,
    Gender: StudentGender,
    Birthday: string,
    Status: StudentStatus,
    Checked: boolean
}
export interface IStudentInfo {
    Group: string,
    Name: string,
    Surname: string,
    Gender: string,
    Birthday: string,
}

export type StudentStatus = 'online' | 'offline'
export type StudentGender = 'F' | 'M'