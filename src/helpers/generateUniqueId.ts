export const generateUniqueId: () => string = () => {
    return new Date().getTime().toString()
}