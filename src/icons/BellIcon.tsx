import React from 'react'

interface PropType {
    isHovered: boolean,
    style: string
}

const BellIcon: React.FC<PropType> = ({ isHovered, style }) => {
    return (
        isHovered ? (
            <img className={style} src='./notification-colored.png' alt='notification' />
            ) : <img className={style} src='./notification-uncolored.png' alt='notification' />
        )
}

export default BellIcon