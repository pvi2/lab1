enum ROUTES {
    DASHBOARD = '/',
    STUDENTS = 'students',
    TASKS = '/tasks'
}

export default ROUTES