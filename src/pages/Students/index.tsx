import React from 'react'
import Header from '../../components/Header'
import Sidebar from '../../components/Sidebar'
import StudentsTable from './components/StudentsTable/StudentsTable'
import styles from './styles.module.css'

const Students: React.FC = () => {
  return (
    <>
      <Header />
      <div className={styles['flex-block']}>
        <Sidebar />
        <div className={styles['students-table-block']}>
          <p className={styles['students-table-block-text']}>Students</p>
          <StudentsTable />
        </div>
      </div>
    </>
  )
}

export default Students