import React from 'react'
import styles from './styles.module.css'

interface PropsType {
    deleteStudent: any
    closeModal: any
    name: string
}

const DeleteModal: React.FC<PropsType> = ({ deleteStudent, closeModal, name }) => {
    return (
        <div className={styles.container}>
            <div className={styles.header}>
                <p className={styles.title}>Warning</p>
                <div className={styles.closeIconContainer} onClick={closeModal}>
                    <img src='./close.png' alt='Close' className={styles.closeIcon} />
                </div>
            </div>
            <div className={styles.body}>
                <p className={styles.text}>Are you sure you want to delete {name}?</p>
            </div>
            <div className={styles.buttons}>
                <button onClick={deleteStudent} className={styles.btn}>
                    OK
                </button>
                <button onClick={closeModal} className={styles.btn}>
                    Cancel
                </button>
            </div>
        </div>
    )
}

export default DeleteModal