import React, { useState, useMemo, useCallback } from 'react'
import styles from './styles.module.css'
import { IStudent, IStudentInfo, StudentGender } from '../../../../interfaces/interfaces'
import StudentItem from '../StudentItem'
import { generateUniqueId } from '../../../../helpers/generateUniqueId'
import Pagination from '../Pagination'
import Modal from '../../../../components/Modal'
import AddStudentModal from '../AddStudentModal'

const studentsArray: IStudent[] = [
    { Id: '1', Group: 'PZ-25', Name: "Anom Surname", Gender: 'F', Birthday: "2023-03-03", Status: 'online', Checked: false },
    { Id: '2', Group: 'PZ-25', Name: "Megha Surname", Gender: 'M', Birthday: "2023-03-03", Status: 'offline', Checked: false },
    { Id: '3', Group: 'PZ-25', Name: "Subham Surname", Gender: 'F', Birthday: "2023-03-03", Status: 'online', Checked: false },
]

const StudentsTable = () => {
    const [data, setData] = useState<IStudent[]>(studentsArray)
    const [modal, setModal] = useState<boolean>(false)
    const [isChecked, setIsChecked] = useState<boolean>(false)

    const addStudent: (student: IStudentInfo) => void = useCallback((student) => {
        const newStudent: IStudent = {
            Id: generateUniqueId(),
            Group: student.Group,
            Name: `${student.Name} ${student.Surname}`,
            Gender: student.Gender === 'Male' ? 'M' : 'F',
            Birthday: student.Birthday,
            Status: 'online',
            Checked: false
        }

        setData(prevData => [...prevData, newStudent])
        setModal(false)
    }, [setData])

    const editStudent: (student: IStudentInfo, studentId: string) => void = useCallback((student, studentId) => {
        const currentStudent = data.find(el => el.Id === studentId)
        const newArray = data.map((el:IStudent) => el.Id === studentId ? ({
            ...el,
            Group: student.Group,
            Name: `${student.Name} ${student.Surname}`,
            Gender: student.Gender === 'Male' ? 'M' : 'F' as StudentGender,
            Birthday: student.Birthday,
        }) : el
        )
        console.log('IsUd', currentStudent)
        if (currentStudent) {
            setData(newArray)
        }
        setModal(false)
    }, [data])

    const deleteStudent: (id: string) => void = useCallback((id) => {
        const filteredArray = data.filter(el => el.Id !== id)

        setData(filteredArray)
    }, [data, setData])

    const mappedArray: React.ReactElement[] = useMemo(() => data.map(val => {
        return (
            <StudentItem editStudent={editStudent} data={data} setData={setData} isAllChecked={isChecked} key={val.Id} student={val} deleteStudent={deleteStudent} />
        )
    }), [data, deleteStudent, isChecked])

    const setAllStudentsChecked: () => void = () => {
        if (isChecked) {
            const newArray = data.map(el => ({ ...el, Checked: !isChecked }))
            setData(newArray)
            setIsChecked(false)
        } else {
            const newArray = data.map(el => ({ ...el, Checked: !isChecked }))
            setData(newArray)
            setIsChecked(true)
        }
    }

    const openModal = useCallback(() => {
        setModal(true)
    }, [setModal])

    const closeModal = useCallback(() => {
        setModal(false)
    }, [setModal])

    console.log('data', data)

    // TODO: setup redux
    return (
        <>
            <div className={styles['table-container']}>
                <div className={styles['add-student-container']}>
                    <button className={styles['add-student-btn']} onClick={openModal}>
                        <img className={styles['add-img']} src='./plus.png' alt='add' />
                    </button>
                </div>
                <table>
                    <tbody>
                        <tr>
                            <th className={styles['table-item']}>
                                <input type="checkbox" checked={isChecked} onChange={setAllStudentsChecked} />
                            </th>
                            <th className={styles['table-item']}>Group</th>
                            <th className={styles['table-item']}>Name</th>
                            <th className={styles['table-item']}>Gender</th>
                            <th className={styles['table-item']}>Birthday</th>
                            <th className={styles['table-item']}>Status</th>
                            <th className={styles['table-item']}>Options</th>
                        </tr>
                        {mappedArray}
                    </tbody>
                </table>
                <Pagination />
            </div>
            <Modal width='650px' setModal={setModal} activeModal={modal}>
                <AddStudentModal btnName='Create' title='Add student' isEdit={false} closeModal={closeModal} addStudent={addStudent} />
            </Modal>
        </>
    )
}

export default StudentsTable