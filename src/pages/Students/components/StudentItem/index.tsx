import React, { useCallback, useState } from 'react'
import { IStudent } from '../../../../interfaces/interfaces'
import Modal from '../../../../components/Modal'
import DeleteModal from '../DeleteModal/DeleteModal'
import AddStudentModal from '../AddStudentModal'
import styles from './styles.module.css'


interface PropsType {
    student: IStudent,
    isAllChecked: boolean
    deleteStudent: (id: string) => void
    data: IStudent[]
    setData: any
    editStudent: any
}


const StudentItem: React.FC<PropsType> = ({ student, deleteStudent, isAllChecked, data, setData, editStudent }: PropsType) => {
    const [modal, setModal] = useState<boolean>(false)
    const [editModal, setEditModal] = useState<boolean>(false)
    const [isChecked, setIsChecked] = useState<boolean>(false)

    const openDeleteModel = useCallback(() => {
        setModal(true)
    }, [setModal])

    const closeModal = useCallback(() => {
        setModal(false)
    }, [setModal])

    const deleteStudentItem = useCallback(() => {
        deleteStudent(student.Id)
    }, [deleteStudent])

    const openEditModal = useCallback(() => {
        setEditModal(true)
    }, [setModal])

    const closeEditModal = useCallback(() => {
        setEditModal(false)
    }, [setModal])

    const setCheckedStudent = () => {
        const currentStudent = data.filter(el => el.Id === student.Id)
        const newArray = data.map(el => el.Id === student.Id ? {...student, Checked: !student.Checked } : el)

        setData(newArray)
    }

    console.log('isAllChecked', isAllChecked)


    return (
        <>
            <tr>
                <td className={styles['table-item']}>
                    <input type="checkbox" checked={student.Checked} onChange={setCheckedStudent} />
                </td>
                <td className={styles['table-item']}>{student.Group}</td>
                <td className={styles['table-item']}>{student.Name}</td>
                <td className={styles['table-item']}>{student.Gender}</td>
                <td className={styles['table-item']}>{student.Birthday}</td>
                <td className={styles['table-item']}>
                    {student.Status === 'online' ? (
                        <div className={styles['red-squre']}></div>
                    ) : (
                        <div className={styles['green-squre']}></div>
                    )}
                </td>
                <td className={styles['table-item']}>
                    <div className={styles['block-img']}>
                        <button className={styles['btn-img']} onClick={openEditModal}>
                            <img className={styles.img} src='./pen.png' alt='edit' />
                        </button>
                        <button className={styles['btn-img']} onClick={openDeleteModel}>
                            <img className={styles.img} src='./close.png' alt='delete' />
                        </button>
                    </div>
                </td>
            </tr>
            <Modal width='500px' setModal={setModal} activeModal={modal}>
                <DeleteModal name={student.Name} closeModal={closeModal} deleteStudent={deleteStudentItem} />
            </Modal>
            <Modal width='650px' setModal={setEditModal} activeModal={editModal}>
                <AddStudentModal editStudent={editStudent} studentId={student.Id} btnName='Save' student={student} title='Edit student' isEdit={true} closeModal={closeEditModal} />
            </Modal>
        </>
    )
}

export default StudentItem