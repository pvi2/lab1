import React, { useMemo } from 'react'
import styles from './styles.module.css'

const Pagination: React.FC = () => {
  const mockArray: string[] = useMemo(() => ['<', '1', '2', '3', '4', '>'], [])
  const renderMockArray = useMemo(() => mockArray.map((el, key) => (
    <div key={key} className={styles.block}>
      <p>{el}</p>
    </div>
  )), [mockArray])

  return (
    <div className={styles.container}>
      <div className={styles['list-block']}>
        {renderMockArray}
      </div>
    </div>
  )
}

export default Pagination