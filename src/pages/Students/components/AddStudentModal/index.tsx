import React, { useState } from 'react'
import { IStudent, IStudentInfo } from '../../../../interfaces/interfaces'
import styles from './styles.module.css'

interface PropsType {
    isEdit: boolean
    student?: IStudent
    closeModal: any
    addStudent?: any
    title: string
    btnName: string
    studentId?: string
    editStudent?: any
}

const AddStudentModal: React.FC<PropsType> = ({ isEdit, closeModal, addStudent, student, title, btnName, editStudent, studentId }) => {
    const [studentInfo, setStudentInfo] = useState<IStudentInfo | null>(
        (isEdit && student ? {
            Group: student.Group,
            Name: student.Name?.split(' ')[0],
            Surname: student.Name?.split(' ')[1],
            Gender: student.Gender === 'M' ? 'Male' : 'Female',
            Birthday: student.Birthday
        } : {
            Group: '',
            Name: '',
            Surname: '',
            Gender: '',
            Birthday: '',
        }
        )
    )

    const changeText = (e: any, name: string) => {
        const { value } = e.target
        const re = /^[A-Za-z]+$/;
        if (name === 'Name' || name === 'Surname' || name === 'gender') {
            if (re.test(value)) {
                setStudentInfo(prevState => {
                    return prevState ? {
                        ...prevState,
                        [name]: value
                    } : null
                })
            } else {
                return
            }
        }
        setStudentInfo(prevState => {
            return prevState ? {
                ...prevState,
                [name]: value
            } : null
        })
    }

    const submit = (e: any) => {
        console.log(11)
        e.preventDefault()
        console.log('----------', isEdit)
        if (isEdit) {
            console.log(studentInfo, studentId)
            editStudent(studentInfo, studentId)
            closeModal()
            console.log('-------------------------sdsdsdsdsd')
        } else {

            const dataSpliced = studentInfo?.Birthday.split('-')
            if (dataSpliced) {
                // let isValidDate = Date.parse(`${dataSpliced[0]}/${dataSpliced[1]}/${dataSpliced[2]}`);
                // console.log()
                if (Number(dataSpliced[0]) < 1940 || Number(dataSpliced[0]) > 2005) {
                    console.log("Not a valid date format.");
                } else {
                    console.log('dataSpliced', dataSpliced)
                    addStudent(studentInfo)
                    setStudentInfo({
                        Group: '',
                        Name: '',
                        Surname: '',
                        Gender: '',
                        Birthday: '',
                    })
                }
            }
            console.log('studentInfo', studentInfo)
        }
    }

    const isButtonDisabled = !studentInfo?.Birthday || !studentInfo.Gender || !studentInfo.Group || !studentInfo.Name || !studentInfo.Surname

    return (
        <form className={styles.container} method="POST">
            <div className={styles.header}>
                <p className={styles.title}>{title}</p>
                <div className={styles.closeIconContainer} onClick={closeModal}>
                    <img src='./close.png' alt='Close' className={styles.closeIcon} />
                </div>
            </div>
            <div className={styles.body}>
                {/* <p className={styles.text}>Are you sure you want to delete {name}?</p> */}
                <div className={styles.inputs}>
                    <div className={styles.inputBlock}>
                        <p className={styles.inputText}>Group</p>
                        <select placeholder='Select Group' value={studentInfo?.Group} className={styles.input} onChange={(e) => changeText(e, 'Group')}>
                            <option value="" disabled selected>Select your group</option>
                            <option>PZ-26</option>
                            <option>PZ-25</option>
                            <option>PZ-24</option>
                            <option>PZ-23</option>
                            <option>PZ-22</option>
                            <option>PZ-21</option>
                        </select>
                    </div>
                    <div className={styles.inputBlock}>
                        <p className={styles.inputText}>First name</p>
                        <input type='' onChange={(e) => changeText(e, 'Name')} className={styles.input} value={studentInfo?.Name} />
                    </div>
                    <div className={styles.inputBlock}>
                        <p className={styles.inputText}>Last name</p>
                        <input onChange={(e) => changeText(e, 'Surname')} className={styles.input} value={studentInfo?.Surname} />
                    </div>
                    <div className={styles.inputBlock}>
                        <p className={styles.inputText}>Gender</p>
                        <select value={studentInfo?.Gender} className={styles.input} onChange={(e) => changeText(e, 'Gender')}>
                            <option value="" disabled selected>Select your gender</option>
                            <option>Male</option>
                            <option>Female</option>
                        </select>
                    </div>
                    <div className={styles.inputBlock}>
                        <p className={styles.inputText}>Birthday</p>
                        <input onChange={(e) => changeText(e, 'Birthday')} className={styles.input} type='date' value={studentInfo?.Birthday} />
                    </div>
                </div>
            </div>
            <div className={styles.buttons}>
                <button
                    type='submit'
                    onClick={(e) => submit(e)}
                    className={isButtonDisabled ? styles.disabledBtn : styles.btn}
                    disabled={isButtonDisabled}
                >
                    {btnName}
                </button>
                <button onClick={closeModal} type='button' className={styles.btn}>
                    Cancel
                </button>
            </div>
        </form>
    )
}

export default AddStudentModal