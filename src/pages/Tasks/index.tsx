import React from 'react'
import Header from '../../components/Header'
import Sidebar from '../../components/Sidebar'

const Tasks: React.FC = () => {
  return (
    <>
    <Header />
    <div>
      <Sidebar />
    </div>
    </>
  )
}

export default Tasks