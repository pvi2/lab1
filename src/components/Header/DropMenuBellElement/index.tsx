import React from 'react'
import styles from './styles.module.css'

interface PropsType {
    name: string;
    imgUrl: string
}

const DropMenuBellElement: React.FC<PropsType> = ({ name, imgUrl }) => {
    return (
        <div className={styles.dropMenuElementContainer}>
            <div className={styles.leftBlock}>
                <img className={styles.userImg} src={imgUrl} alt="userPhoto" />
                <p className={styles.dropMenuElementText}>{name}</p>
            </div>
            <div className={styles.messageBlock}></div>
        </div>
    )
}

export default DropMenuBellElement
