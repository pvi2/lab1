import React, { useState, useCallback, useMemo } from 'react'
import BellIcon from '../../icons/BellIcon'
import DropModal from '../DropModal'
import DropMenuBellElement from './DropMenuBellElement'
import DropMenuElement from './DropMenuProfileElement'
import styles from './styles.module.css'

const Header: React.FC = () => {
  const [isHovered, setIsHovered] = useState<boolean>(false)
  const [isProfileDropMenuOpen, setIsProfileDropMenuOpen] = useState<boolean>(false)
  const [isBellDropMenuOpen, setIsBellDropMenuOpen] = useState<boolean>(false)

  const onMouseEnter = useCallback(() => {
    setIsHovered(true)
  }, [])

  const onMouseLeave = useCallback(() => {
    setIsHovered(false)
  }, []) 

  const openProfileDropMenu = useCallback(() => {
    setIsProfileDropMenuOpen(true)
  }, [])

  const closeProfileDropMenu = useCallback(() => {
    setIsProfileDropMenuOpen(false)
  }, [])

  const openBellDropMenu = useCallback(() => {
    setIsBellDropMenuOpen(true)
  }, [])

  const closeBellDropMenu = useCallback(() => {
    setIsBellDropMenuOpen(false)
  }, [])


  const profileDropMenu = useMemo(() => isProfileDropMenuOpen && (
    <div onMouseEnter={openProfileDropMenu} onMouseLeave={closeProfileDropMenu}>
        <DropModal style={styles.dropMenuProfile}>
            <DropMenuElement text='Profile' />
            <DropMenuElement text='Log Out' />
        </DropModal>
    </div>
), [isProfileDropMenuOpen, openProfileDropMenu, closeProfileDropMenu])

const bellDropMenu = useMemo(() => isBellDropMenuOpen && (
  <div onMouseEnter={openBellDropMenu} onMouseLeave={closeBellDropMenu}>
      <DropModal style={styles.dropMenuBell}>
          <DropMenuBellElement name='Admin K.' imgUrl='./user.png'/>
          <DropMenuBellElement name='Admin K.' imgUrl='./user.png'/>
          <DropMenuBellElement name='Admin K.' imgUrl='./user.png'/>
      </DropModal>
  </div>
), [isBellDropMenuOpen, openBellDropMenu, closeBellDropMenu])


  return (
    <header className={styles.container}>
      <div className='left-block'>
        <p className={styles['left-block-text']}>CMS</p>
      </div>
      <div className={styles['rigth-block']}>
        <div
          className={styles['bell-icon-block']}
          onMouseEnter={openBellDropMenu}
          onMouseLeave={closeBellDropMenu}
        >
          <BellIcon isHovered={isBellDropMenuOpen} style={styles['bell-icon']} />
          <div className={styles['red-circle']}></div>
          {bellDropMenu}
        </div>
        <div className={styles['rigth-block-avatar']} onMouseEnter={openProfileDropMenu} onMouseLeave={closeProfileDropMenu}>
          <div className={styles['user-icon-block']}>
            <img className={styles['rigth-block-icon']} src='./user.png' alt='profile' />
          </div>
          <p className={styles['rigth-block-text']}>James Bond</p>
          {profileDropMenu}
        </div>
      </div>
    
    </header>
  )
}

export default Header