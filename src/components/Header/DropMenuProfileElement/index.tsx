import React from 'react'
import styles from './styles.module.css'

interface PropsType {
  text: string;
}

const DropMenuElement: React.FC<PropsType> = ({ text }) => {
  return (
    <div className={styles.dropMenuElementContainer}>
      <p className={styles.dropMenuElementText}>{text}</p>
    </div>
  )
}

export default DropMenuElement
