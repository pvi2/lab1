import React from 'react'
import { Link } from 'react-router-dom'
import ROUTES from '../../constants/routes'
import styles from './styles.module.css'
import { useLocation } from 'react-router-dom'

const Sidebar: React.FC = () => {
  const { pathname } = useLocation()
  return (
    <div className={styles.container}>
      <div className={styles.list}>
        <Link to={ROUTES.DASHBOARD}>
          <p style={{fontWeight: !pathname.includes(ROUTES.STUDENTS) && !pathname.includes(ROUTES.TASKS) ? 'bold' : 'normal'}} className={styles['link-text']}>Dashboard</p>
        </Link>
        <Link to={`/${ROUTES.STUDENTS}`}>
          <p style={{fontWeight: pathname.includes(ROUTES.STUDENTS) ? 'bold' : 'normal'}} className={styles['link-text']}>Students</p>
        </Link>
        <Link to={ROUTES.TASKS}>
          <p style={{fontWeight: pathname.includes(ROUTES.TASKS) ? 'bold' : 'normal'}} className={styles['link-text']}>Tasks</p>
        </Link>
      </div>
    </div>
  )
}

export default Sidebar