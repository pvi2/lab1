import React from 'react'
import styles from './styles.module.css'

type PropsType = {
    width: string
    children: React.ReactNode,
    activeModal: boolean,
    setModal: any
}

const Modal: React.FC<PropsType> = ({ children, width, activeModal, setModal }) => {


    const toggleModal: () => void = () => {
        setModal(false)
    }

    return (
        <div onClick={toggleModal} className={`${styles.windowBlock} ${activeModal ? styles.active : styles.hidden}`}>
            <div style={{width: width}} onClick={(e) => e.stopPropagation()} className={`${styles.innerBlock} ${activeModal ? styles.activeModal : styles.hiddenModal}`}>
                {children}
            </div>
        </div>
    )
}

export default React.memo(Modal)