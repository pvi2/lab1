import React from 'react';
import styles from './styles.module.css'

interface DropMenuProps  {
  children: React.ReactNode;
  style?: string;
}

const DropMenu: React.FC<DropMenuProps> = React.memo(({ children, style }) => {
    return (
        <div className={`${styles.container} ${style}`}>{children}</div>
    )
})

export default DropMenu
