import React, { Suspense } from 'react';
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import ROUTES from './constants/routes'
import Dashboard from './pages/Dashboard';
import Students from './pages/Students'
import Tasks from './pages/Tasks'
import './App.css'

const App: React.FC = () => {
  return (
    <Suspense fallback={<p>Loading....</p>}>
      <Router>
        <Routes>
          <Route path={ROUTES.DASHBOARD} element={<Dashboard />} />
          <Route path={ROUTES.STUDENTS} element={<Students />} />
          <Route path={ROUTES.TASKS} element={<Tasks />} />
        </Routes>
      </Router>
    </Suspense>
  );
}

export default App;
